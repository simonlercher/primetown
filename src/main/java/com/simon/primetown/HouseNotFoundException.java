package com.simon.primetown;



import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class HouseNotFoundException extends Exception {


    private static final long serialVersionUID = -627116577313731942L;




}
