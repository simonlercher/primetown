package com.simon.primetown;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HouseController {

    @Autowired
    HouseRepository repo;


    @GetMapping("/house")
    public List<House> getHouses() {
        return repo.findAll();
    }

    @GetMapping("/house/{id}")
    public House getHouse(@PathVariable Long id) throws HouseNotFoundException {
        return repo.findById(id).orElseThrow(() -> new HouseNotFoundException());
    }

    @GetMapping("/house/size")
    public long getSize() {
        return repo.count();
    }


    @PostMapping("/house")
    public void addHouse(@RequestBody House house) {
        repo.save(house);
    }

    @DeleteMapping("/house/{id}")
    public void deleteHouse(@PathVariable Long id){
        repo.deleteById(id);
    }



}