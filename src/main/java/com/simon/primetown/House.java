package com.simon.primetown;

import javax.persistence.Entity;
        import javax.persistence.GeneratedValue;
        import javax.persistence.GenerationType;
        import javax.persistence.Id;
        import javax.persistence.Table;
import java.util.Random;


@Entity
@Table(name = "house")
public class House{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String owner;
    private int yearOfConstruction;
    private int number;




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getYearOfConstruction() {

        Random rand = new Random();
        int randzahl;
        randzahl= 1800 + rand.nextInt(220);
        yearOfConstruction = randzahl;
        return yearOfConstruction;
    }

    public void setYearOfConstruction(int yearOfConstruction) {
        this.yearOfConstruction = yearOfConstruction;
    }

    public int getNumber() {

        number = 7;
        int i = 1;
        Random rand = new Random();
        int randomzahl;

        randomzahl = rand.nextInt(10000);
        boolean primzahl= true;

        while (i<1000){
            for(int j=2; j<i-1;j++){

                int ergebnis = (i*randomzahl)%j;
                if(ergebnis == 0){
                    primzahl = false;
                }
                else{
                    primzahl=true;
                    ergebnis=number;


                }

                i++;
            }
        }

        return number;

    }

    public void setNumber(int number) {
        this.number = number;
    }

    public House(){}


}